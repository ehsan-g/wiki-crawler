from tiwall.tiwall.spiders.xpaths import xpather
from models import Type, Location
from app import dbsession


def type(event_scrapy_selector, event, target_name):
    xpaths = xpather(target_name)
    event_type = event_scrapy_selector.xpath(xpaths['event_type_xpath']).extract()
    event_cat = event_scrapy_selector.xpath(xpaths['event_cat_xpath']).extract()
    event_genre = event_scrapy_selector.xpath(xpaths['event_genre_xpath']).extract()

    loc_objs = dbsession.query(Location).all()
    for loc in loc_objs:
        event_cat = [x for x in event_cat if not loc.city in x]

    if event_type:
        event_types = event_type
    else:
        event_types = None

    if event_cat:
        categories = event_cat
    else:
        categories = None

    if event_genre:
        genres = event_genre
    else:
        genres = None

    if event.types:
        type = event.types[0]

    else:
        type = Type(event_id=event.id, event_types=event_types, categories=categories, genres=genres)
        dbsession.add(type)

    dbsession.commit()
    # Since URLType is set in model the link is furl object
    print(f"\n Types of event: {event.id} " + type.event_types[0])



