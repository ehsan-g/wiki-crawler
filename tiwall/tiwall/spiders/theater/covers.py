from app import dbsession
from tiwall.tiwall.spiders.xpaths import xpather


def cover(event_scrapy_selector, event, target_name):
    xpaths = xpather(target_name)
    event_cover_url = event_scrapy_selector.xpath(xpaths['event_cover_xpath']).extract()
    default_cover = ['https://zbcdn.cloud/files/cache/90651_theater-cover.3.498535.jpg']
    if event_cover_url and default_cover != event_cover_url:
        event.cover = event_cover_url[0]
        dbsession.commit()
        # Since URLType is set in model the link is furl object
        print(f"\n cover's link of event:{event.id}" + event.cover.url)



