import re
import time
from app import dbsession
from models import Location
from tiwall.tiwall.spiders.xpaths import xpather


def location(event_scrapy_selector, event, target_name):
    xpaths = xpather(target_name)
    event_loc_name = event_scrapy_selector.xpath(xpaths['event_loc_name_xpath']).extract()
    event_loc_city = event_scrapy_selector.xpath(xpaths['event_loc_city_xpath']).extract()
    event_loc_address = event_scrapy_selector.xpath(xpaths['event_loc_address_xpath']).extract()

    if target_name == 'tiwall':
        #  google map might take time to load so try one more time
        w = 0
        while w <= 3:
            event_lat_long = event_scrapy_selector.xpath(xpaths['event_lat_long_xpath']).extract()
            try:
                lat_long = re.findall("\d+\.\d+", event_lat_long[0])
                lat = lat_long[0]
                long = lat_long[1]
                break
            except:
                print('waiting for google map\t' + str(w))
            lat = 0
            long = 0
            time.sleep(1)
            w += 1
    else:
        lat = 0
        long = 0

    if event_loc_name:
        location_name = event_loc_name[0]
    else:
        location_name = 'N/A'

    if event_loc_city:
        city = event_loc_city[0]
    else:
        city = 'N/A'

    if event_loc_address:
        address = event_loc_address[0]
        regex = re.compile(r'[\n\r\t<>]')
        address = regex.sub(" ", address).strip()  # clean the item from \n ,
    else:
        address = 'N/A'

    query = dbsession.query(Location).filter_by(event_id=event.id).first()
    if not query:
        loc = Location(event_id=event.id, location_name=location_name, city=city, address=address, lat=lat, long=long)
        dbsession.add(loc)

    else:
        print('Location already Exist in')
        locations = event.locations[0]
        locations.location_name = location_name
        locations.city = city
        locations.address = address
        locations.lat = lat
        locations.long = long

    dbsession.commit()

    # Since URLType is set in model the link is furl object
    print(f"\n location of the event: {str(event.id)}" + location_name)



