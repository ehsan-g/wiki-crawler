from app import dbsession
from tiwall.tiwall.spiders.xpaths import xpather



def title(event_scrapy_selector, event, target_name):
    xpaths = xpather(target_name)
    event_title = event_scrapy_selector.xpath(xpaths['event_title_xpath']).extract()
    if event_title:
        event.title = event_title[0]
        dbsession.commit()
        print(f'\n Title of event: {event.id} ' + event.title)
