import re
from persiantools import digits
from persiantools.jdatetime import JalaliDate
from app import dbsession
from tiwall.tiwall.spiders.xpaths import xpather
from models import Schedule


def schedule(event_scrapy_selector, event, target_name):
    xpaths = xpather(target_name)
    event_date = event_scrapy_selector.xpath(xpaths['event_date_xpath']).extract()
    # days = {'۰': 0, '۱': 1, '۲': 2, '۳': 3, '۴': 4, '۵': 5, '۶': 6, '۷': 7, '۸': 8, '۹': 9}
    weekdays = {'شنبه', 'یک شنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه'}

    months = {'فروردین': ['1', 'فروردین‌ماه', 31], 'اردیبهشت': ['2', 'اردیبهشت‌ماه', 31], 'خرداد': ['3', 'خرداد‌‌ماه', 31], 'تیر': ['4', 'تیر‌‌ماه', 31],
              'مرداد': ['5', 'مرداد‌‌ماه', 31], 'شهریور': ['6', 'شهریور‌ماه', 31], 'مهر': ['7', 'مهر‌ماه', 30], 'آبان': ['8', 'آبان‌‌ماه', 30],
              'آذر': ['9', 'آذر‌ماه', 30], 'دی': ['10', 'دی‌‌ماه', 30], 'بهمن': ['11', 'بهمن‌ماه', 30], 'اسفند': ['12', 'اسفند‌‌ماه', 29, 30]}

    # years = {'۱۳۸۰': 1380, '۱۳۸۱': 1381, '۱۳۸۲': 1382, '۱۳۸۳': 1383, '۱۳۸۴': 1384, '۱۳۸۵':1385, '۱۳۸۶': 1386,
    #          '۱۳۸۷': 1387, '۱۳۸۸': 1388, '۱۳۸۹': 1389, '۱۳۹۰': 1390, '۱۳۹۱': 1391, '۱۳۹۲': 1392,
    #          '۱۳۹۳': 1393, '۱۳۹۴': 1394, '۱۳۹۵': 1395, '۱۳۹۶': 1396, '۱۳۹۷': 1397, '۱۳۹۸': 1398, '۱۳۹۹': 1399,
    #          '۱۴۰۰': 1400}
    if event_date:
        regex = re.compile(r'[\n\r\t0123456789?<U+>]')
        clean_event_schedules = regex.sub(" ", event_date[0]).strip()
        words = clean_event_schedules.split()

        now = JalaliDate.today()
        current_year_english = now.strftime('%Y')
        current_year_farsi = digits.en_to_fa(current_year_english)
        current_year = str(current_year_farsi)

        ''' Possible words
        مردادماه ۱۳۹۹
        اسفند ۹۸
        از اسفند ۱۳۹۸
        
        شنبه ۱۰ اسفند ۱۳۹۸
        پنجشنبه ۲۳ مرداد
        
        ۰۵ تا ۱۵ اسفند ۱۳۹۸
        
        ۰۱ مهر تا ۰۳ شهریور
        
        چهارشنبه ۱۴ تا ۱۶ اسفند ۱۳۹۸
        
        پنجشنبه ۱۱ تا ۱۲ اردیبهشت 
        
        فروردین و اردیبهشت ماه ۱۳۹۹
        '''

        count = len(words)


        # از اسفند ۱۳۹۸
        if 2 <= count <= 3 and words[1] in months.keys():
            start_day = '۱'
            start_month = words[1]
            start_year = words[2]
            end_day = '۲۹'
            end_month = words[1]
            end_year = words[2]

        # مردادماه ۱۳۹۹
        elif count == 2 and any("ماه" in x for x in words):
            start_day = '۱'
            for month in months.keys():
                if month + "ماه" == words[0]:
                    start_month = month
            start_year = words[1]
            end_day = '۲۹'
            end_month = start_month
            end_year = start_year

        #  شنبه ۱۰ اسفند ۱۳۹۸
        elif count == 4 and words[0] in weekdays:
            start_day = words[1]
            start_month = words[2]
            start_year = words[3]
            end_day = start_day
            end_month = start_month
            end_year = start_year

        # پنجشنبه ۲۳ مرداد
        elif count == 3 and words[0] in weekdays:
            start_day = words[1]
            start_month = words[2]
            start_year = current_year
            end_day = start_day
            end_month = start_month
            end_year = start_year

        # ۰۵ تا ۱۵ اسفند ۱۳۹۸
        elif count == 5 and words[1] == 'تا':
            start_day = words[0]
            start_month = words[3]
            start_year = words[4]
            end_day = words[2]
            end_month = words[3]
            end_year = words[4]

        # ۰۱ مهر تا ۰۳ شهریور
        elif count == 5 and words[2] == 'تا' and event.page < 3 and words[1] in months.keys():
            start_day = words[0]
            start_month = words[1]
            start_year = current_year
            end_day = words[3]
            end_month = words[4]
            end_year = start_year

        # پنجشنبه ۱۱ تا ۱۲ اردیبهشت
        elif count == 5 and words[2] == 'تا' and words[0] in weekdays:
            start_day = words[1]
            start_month = words[4]
            start_year = current_year
            end_day = words[3]
            end_month = start_month
            end_year = start_year

        # چهارشنبه ۱۴ تا ۱۶ اسفند ۱۳۹۸
        elif count == 6 and words[2] == 'تا' and words[0] in weekdays:
            start_day = words[1]
            start_month = words[4]
            start_year = words[5]
            end_day = words[3]
            end_month = start_month
            end_year = start_year

        # فروردین و اردیبهشت ماه ۱۳۹۹
        elif count == 5 and words[1] == 'و' and words[2] in months.keys():
            start_day = '۱'
            start_month = words[0]
            start_year = words[4]
            end_day = '۱'
            end_month = words[2]
            end_year = words[4]

        else:
            start_day = '۱'
            start_month = 'فروردین'
            start_year = '۱۴۰۰'
            end_day = '۱'
            end_month = 'فروردین'
            end_year = '۱۴۰۰'

    else:
        start_day = '۱'
        start_month = 'فروردین'
        start_year = '۱۴۰۰'
        end_day = '۱'
        end_month = 'فروردین'
        end_year = '۱۴۰۰'

    print(start_year + start_month + start_day + '\t\t' + end_year + end_month + end_day)
    start_day = int(digits.fa_to_en(start_day))
    start_month = int(digits.fa_to_en(months[start_month][0]))
    start_year = int(digits.fa_to_en(start_year))
    end_day = int(digits.fa_to_en(end_day))
    end_month = int(digits.fa_to_en(months[end_month][0]))
    end_year = int(digits.fa_to_en(end_year))

    start_date = JalaliDate(start_year, start_month, start_day).to_gregorian()
    end_date = JalaliDate(end_year, end_month, end_day).to_gregorian()

    if not event.schedules:
        sched = Schedule(event_id=event.id, start_date=start_date, end_date=end_date)
        event.schedules.append(sched)
        dbsession.commit()

    else:
        sched = dbsession.query(Schedule).filter_by(event_id=event.id).first()
        sched.start_date = start_date
        sched.end_date = end_date
        dbsession.commit()


    # print(f'\n schedule of event: {event.id} ' + event.schedules.start_date + '\t' + event.schedules.end_date)

