import re
from validate import *
from tiwall.tiwall.spiders.xpaths import xpather


def crawlobj(clean_role, event):
    crawl_obj = CrawledRole(unsorted_role=clean_role, event_id=event.id)
    # every event has only one from all roles
    if crawl_obj not in event.positions:
        dbsession.add(crawl_obj)
        dbsession.commit()
    return crawl_obj


def wikiobj(wiki_role, event):
    wiki_obj = WikiRole(wiki_role=wiki_role, event_id=event.id)
    dbsession.add(wiki_obj)
    dbsession.commit()
    return wiki_obj


def wikicrawl(crawl_obj, wiki_obj):
    wiki_obj.crawls.append(crawl_obj)
    dbsession.commit()
    return wikicrawl


def role(event_scrapy_selector, event, target_name):
    xpaths = xpather(target_name)
    roles = event_scrapy_selector.xpath(xpaths['event_position_xpath']).extract()
    print('\n Dirty roles of this event: ' + str(roles))
    # remove extra
    cleaning_roles = [x for x in roles if not '📍' in x]
    cleaning_roles = [x for x in cleaning_roles if not '📅' in x]
    cleaning_roles = [x for x in cleaning_roles if not '⏰' in x]
    cleaning_roles = [x for x in cleaning_roles if not '💳' in x]
    cleaning_roles = [x for x in cleaning_roles if not '⏳' in x]
    cleaning_roles = [x for x in cleaning_roles if not 'نشانی:' in x]
    cleaning_roles = [x for x in cleaning_roles if not 'بها:' in x]
    cleaning_roles = [x for x in cleaning_roles if not 'خلاصه:' in x]
    cleaning_roles = [x for x in cleaning_roles if not 'طرح بهای بلیت:' in x]

    print('Now the crawled role is as clean as it gets: ' + str(cleaning_roles) + '\n')
    for x in cleaning_roles:
        regex = re.compile(r'[\n\r\t0123456789?<U+>]')
        clean_role = regex.sub(" ", x).strip()  # clean the item from \n , \t + Remove spaces
        rolling(clean_role, event)
    print('\n-------------------------------------Roles are Done -----------------------------------\n')


def rolling(clean_role, event):
    print('--------------------------------\tChecking this role:\t' + clean_role + '\tin this event' + str(event.id))
    # check for all the keys in validate.py e.g. 'actor':'بازیگران'
    for key in role_schema.keys():
        document = {str(key): clean_role}
        # role_schema[dramaturge]['allowed'][0], ...
        wiki_role = (role_schema[str(key)]['allowed'][0])
        validated_crawl_roles = wikier(wiki_role)
        if validated_crawl_roles:
            for validated_role in validated_crawl_roles:
                # update schema for next roles
                role_schema[str(key)]['allowed'].append(validated_role)
        crawl_obj = dbsession.query(CrawledRole).filter_by(unsorted_role=clean_role).filter_by(event_id=event.id).first()
        wiki_obj = dbsession.query(WikiRole).filter_by(wiki_role=wiki_role).first()

        # not in schema
        if not v.validate(document, role_schema):
            print('\t\t\t\t\t\t\t----- Not in schema\t' + wiki_role + '\n')
            # if could not validate add it to allowed in schema e.g. راننده
            if crawl_obj:
                if crawl_obj not in event.positions:
                    event.positions.append(crawl_obj)
                    dbsession.commit()
                elif crawl_obj in event.positions:
                    continue
            else:
                crawlobj(clean_role, event)
                print('Created the new crawl_obj\t' + clean_role + '\tPlease select the wiki_role from panel\t' + clean_role)

        # exists in schema
        else:
            print('\t\t\t\t\t\t\t----- already exists in schema\t' + wiki_role + '\n')
            if crawl_obj:
                print('This crawl_obj:\t' + clean_role + '\talready exists in db with id of:\t' + str(crawl_obj.id))
                if wiki_obj:
                    query = dbsession.query(CrawledWiki).filter_by(crawl_id=crawl_obj.id).first()
                    if query and query.wiki_role_id == wiki_obj.id and query.crawl_id == crawl_obj.id:
                        print("The relation already exists\t" + clean_role + '\tand\t' + wiki_role + '\t awwwww <3<3<3')
                    else:
                        wikicrawl(crawl_obj, wiki_obj)
                        print('Now in rel with\t' + clean_role + '\tand\t' + wiki_role)
                else:
                    wiki_obj = wikiobj(wiki_role, event)
                    wikicrawl(crawl_obj, wiki_obj)
                    print('Created the new wiki_role\t' + wiki_role + '\tNow in rel\t' + clean_role + '\tand\t' + wiki_role)

            else:
                crawl_obj = crawlobj(clean_role, event)
                if wiki_obj:
                    print('Created the new crawl_role\t' + clean_role + '\tNow in rel\t' + clean_role + '\tand\t' + wiki_role)

                else:
                    wiki_obj = wikiobj(wiki_role, event)
                    print('Created the new crawl_role, wiki_role and rel b/w:\t' + clean_role + '\tand\t' + wiki_role)
                wikicrawl(crawl_obj, wiki_obj)
    # create xpath for diff positions to find people for that position

