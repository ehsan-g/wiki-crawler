import re
import sys

from models import Person, Event, CrawledRole
from app import dbsession
from tiwall.tiwall.spiders.xpaths import xpather2


def check_person_role(person_obj, crawl_obj, event):
    # make sure a person with same role is saved only once
    if crawl_obj is None:
        print('You have not saved the role to the database yet!')
        sys.exit()
    else:
        people = crawl_obj.crawl_people
        # might no person was added before
        if not people:
            crawl_obj.crawl_people.append(person_obj)
            dbsession.commit()
        else:
            for person in people:
                if person_obj.first_name == person.first_name and person_obj.last_name == person.last_name and person_obj.event_id == event.id:
                    continue
                else:
                    # add the person to this specific role
                    crawl_obj.crawl_people.append(person_obj)
                    dbsession.commit()

    print(f"\n person of event:\t{event.id}\t" + crawl_obj.unsorted_role + "\t" + person_obj.last_name + '\t' + person_obj.first_name)


def people(event_scrapy_selector, event, target_name):
    xpaths = xpather2(target_name, event)

    count_roles = len(xpaths)

    for i in range(count_roles//2):
        event_people = event_scrapy_selector.xpath(xpaths['event_people_xpath' + str(i)]).extract()
        role = xpaths['role' + str(i)]
        try:
            event_people = event_people[0].split('،')
        except AttributeError:
            pass

        for x in event_people:
            count = 0
            regex = re.compile(r'[\n\r\t0123456789?<U+>:xa,،]')
            x = regex.sub(" ", x).strip()
            # split char
            splt_char = " "
            for i in x:
                if i == ' ':
                    count += 1
            # initializing K
            k = count
            if k > 0:
                # Using split() + join()
                # Split string on Kth Occurrence of Character
                temp = x.split(splt_char)
                res = splt_char.join(temp[:k]), splt_char.join(temp[k:])
                first_name = res[0]
                last_name = res[1]
            else:
                first_name = ''
                last_name = ''

            crawl_obj = dbsession.query(CrawledRole).filter_by(unsorted_role=role).filter_by(event_id=event.id).first()
            person_obj = dbsession.query(Person).filter_by(first_name=first_name).filter_by(last_name=last_name).first()

            if not person_obj:
                person_obj = Person(event_id=event.id, first_name=first_name, last_name=last_name)
                dbsession.add(person_obj)
                dbsession.commit()
                check_person_role(person_obj, crawl_obj, event)

            else:
                check_person_role(person_obj, crawl_obj, event)
