from scrapy.selector import Selector
from selenium import webdriver
from time import sleep
import re


driver = webdriver.Chrome('/tiwall/tiwall/spiders/chromedriver')
page = 0
while page >= 10:
    print('\n\n-------------------- Going to the page: ' + str(page) + ' --------------------\n\n')
    driver.get('https://www.tiwall.com/showcase?filters=s:theater&p='+str(page))  # Theater pagination
    page += 1

    scrapy_selector = Selector(text=driver.page_source)
    events_selector = scrapy_selector.xpath('//*[@class="item-page"]')
    print('Theres a total of ' + str(len(events_selector)) + ' links.')
    event_urls_list = []
    try:
        s = 0
        for event in events_selector:
            url = event.xpath('//*[@class = "item-page"]//@href').extract()[s]
            event_url = 'https://www.tiwall.com' + url
            event_urls_list.append(event_url)
            s = s+1

    except:
        print('Reached last iteration #' + str(s))

    e = 1
    for event_url in event_urls_list:
        print('------------------- Theater Events ------------- # ' + str(e))
        print(event_url)
        driver.get(event_url)
        e += 1
        sleep(2)
        event_scrapy_selector = Selector(text=driver.page_source)

        # Event name
        event_name = event_scrapy_selector.xpath('//*[@class="tooltip no-border first"]//@title')[0].extract()
        print(event_name)

        # Venue
        # Pre-Cleaned Venue Name
        event_venue_pre = event_scrapy_selector.xpath('//*[@class="box_large page event"]/div/a/text()').extract()
        try:
            event_venue = [x for x in event_venue_pre if "\t" not in x][0]  # Clean the list from \n , \t
            print(event_venue)
        except:
            print('Venue Is Not Available')


        event_date_pre = event_scrapy_selector.xpath('//*[@class="theater-date"]/text()')[0].extract()
        try:
            regex = re.compile(r'[\n\r\t]')
            event_date = regex.sub(" ", event_date_pre).strip()  # Clean the item from \n , \t + Remove spaces
            print(event_date)
        except:
            print('Date is Not Set')



        # Director   گروه کارگردانی
        try:
            event_director = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[text()="\nکارگردان"]/following-sibling::text()[1]')[0].extract()).strip(':')
            print('کارگردان' + ':' + event_director)

        except:
            try:
                event_director = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[text()="کارگردان"]/following-sibling::text()[1]')[0].extract()).strip(':')
                print('کارگردان' + ':' + event_director)

            except:
                try:
                    event_director = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[contains(text(),"بازیگر، طراح و کارگردان")]/following-sibling::text()[1]')[0].extract()).strip(':')
                    print('بازیگر، طراح و کارگردان' + ':' + event_director)

                except:
                    try:
                        event_director = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[contains(text(),"نویسنده و کارگردان")]/following-sibling::text()[1]')[0].extract()).strip(':')
                        print('نویسنده و کارگردان' + ':' + event_director)

                    except:
                        event_director = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[contains(text(),"طراح و کارگردان")]/following-sibling::text()[1]')[0].extract()).strip(':')
                        print('طراح و کارگردان' + ':' + event_director)



        # Director's Advisors
        try:
            event_director_adv = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[contains(text(),"مشاور کارگردان")]/following-sibling::text()[1]')[0].extract()).strip(':')
            event_director_adv_split = event_director_adv.split('،')
            for x in event_director_adv_split:
                print('مشاور کارگردان' + ':' + x)
        except IndexError:
            print('No Advisor')


        # Cast بازیگران
        try:
            event_cast = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[contains(text(),"بازیگر")]/following-sibling::text()[1]')[0].extract()).strip(':')
            event_cast_split = event_cast.split('،')
            for x in event_cast_split:
                print('بازیگر' + ':' + x)

        except IndexError:
            print('No Cast')



        # Producers تهیه کننده
        try:
            event_producer = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[contains(text(),"تهیه کننده")]/following-sibling::text()[1]')[0].extract()).strip(':')
            event_producer_split = event_producer.split('،')
            for x in event_producer_split:
                print('تهیه کننده' + ':' + x)
        except IndexError:
            print('No Producer')



        # Playwright
        try:
            event_writer = (event_scrapy_selector.xpath('//*[@class="box_large page event"]//label[contains(text(),"نویسنده")]/following-sibling::text()[1]')[0].extract()).strip(':')
            event_writer_split = event_writer.split('،')
            for x in event_writer_split:
                print('نویسنده' + ':' + x)
        except IndexError:
            print('No Producer')


