from app import dbsession
from tiwall.tiwall.spiders.xpaths import xpather


def poster(event_scrapy_selector, event, target_name):
    xpaths = xpather(target_name)
    event_poster_url = event_scrapy_selector.xpath(xpaths['event_poster_xpath']).extract()
    if event_poster_url:
        event.poster = event_poster_url[0]
        dbsession.commit()
        # Since URLType is set in model the link is furl object
        print(f"\n poster's link of event: {event.id} " + event.poster.url)



