import time

import schedule
from scrapy import Selector
from selenium import webdriver
from models import *
from app import dbsession
from tiwall.tiwall.spiders.xpaths import xpather
from tiwall.tiwall.spiders.mediums import theater


def scheduler(target_name):
    count = dbsession.query(Event).count()
    xpaths = xpather(target_name)
    update_theater(target_name, count, xpaths)
    schedule.every().day.at("00:00").do(update_theater, target_name=target_name, count=count, xpaths=xpaths)
    schedule.every().day.at("08:00").do(update_theater, target_name=target_name, count=count, xpaths=xpaths)
    schedule.every().day.at("14:00").do(update_theater, target_name=target_name, count=count, xpaths=xpaths)
    schedule.every().day.at("18:00").do(update_theater, target_name=target_name, count=count, xpaths=xpaths)
    schedule.every().tuesday.at("18:00").do(update_theater_all, target_name=target_name, count=count, xpaths=xpaths)
    now = 0
    while True:
        # Checks whether a scheduled task is pending to run or not
        schedule.run_pending()
        time.sleep(1)
        print(' it has been\t' + str(now) + '\t seconds that I have been up')
        now += 1


def update_theater_all(target_name, count, xpaths):
    start_page = xpaths['start_from_page']
    end_page = start_page + 1000
    finder = event_finder(target_name, start_page, end_page)
    driver = finder[0]
    event_objs = finder[1]
    theater(target_name, driver, event_objs, count)


'''
Change the end page number below to stop
at the end of that page
'''
def update_theater(target_name, count, xpaths):
    start_page = xpaths['start_from_page']
    end_page = start_page + 2
    finder = event_finder(target_name, start_page, end_page)
    driver = finder[0]
    event_objs = finder[1]
    theater(target_name, driver, event_objs, count)


def event_finder(target_name, start_page, end_page):
    xpaths = xpather(target_name)
    driver = webdriver.Chrome('/Users/ehsan/Documents/Git/Tiwall-Spider/tiwall/tiwall/spiders/chromedriver')
    website_url = xpaths['website']
    event_xpath = xpaths['event_xpath']
    event_url_xpath = xpaths['event_url_xpath']
    start_page_url = xpaths['start_page_url']
    page = start_page

    # loop in a while until you reach a page with not event
    while page <= end_page:
        pagination = start_page_url + (str(page))
        driver.get(pagination)
        scrapy_selector = Selector(text=driver.page_source)
        events_selector = scrapy_selector.xpath(event_xpath)
        if events_selector:
            x = 0
            print('\n\n----------------- Checking page: ' + (str(page)) + ' -----------------\t' + pagination)
            for event_page in events_selector:
                link = event_page.xpath(event_url_xpath).extract()[x]
                event_url = website_url + link
                # first create/update an object from every single link in the target website
                event = dbsession.query(Event).filter_by(link=event_url).first()
                if not event:
                    event = Event(link=event_url, page=page)
                    dbsession.add(event)
                    dbsession.commit()
                else:
                    event.page = page
                    dbsession.commit()
                x += 1
            print('\tThere is a total of\t' + str(x) + '\tlinks in page\t' + str(page))
            page += 1

        else:
            end_page = page
            print('No more page to crawl')
    event_objs = dbsession.query(Event).all()
    return driver, event_objs

