from scrapy.selector import Selector
from tiwall.tiwall.spiders.theater.roles import role
from tiwall.tiwall.spiders.theater.titles import title
from tiwall.tiwall.spiders.theater.posters import poster
from tiwall.tiwall.spiders.theater.covers import cover
from tiwall.tiwall.spiders.theater.locations import location
from tiwall.tiwall.spiders.theater.types import type
from tiwall.tiwall.spiders.theater.schedules import schedule
from tiwall.tiwall.spiders.theater.people import people
from models import *
from app import dbsession

'''
update daily for new events and update all every tuesday
'''


def theater(target_name, driver, event_objs, count):
    for event_obj in event_objs:
        print('\n\n\t------- Theater Events --------- #\t' + str(event_obj.id) + '\tout of ' + str(count))
        print('Checking event_id:\t' + str(event_obj.id) + '\tin page\t' + str(event_obj.page))
        event_url = event_obj.link
        # Since URLType is set in model the link is furl object
        driver.get(event_url.url)
        event_scrapy_selector = Selector(text=driver.page_source)

        # take care of the roles, title, schedule, ...
        title(event_scrapy_selector, event_obj, target_name)
        schedule(event_scrapy_selector, event_obj, target_name)
        poster(event_scrapy_selector, event_obj, target_name)
        cover(event_scrapy_selector, event_obj, target_name)
        location(event_scrapy_selector, event_obj, target_name)
        role(event_scrapy_selector, event_obj, target_name)
        people(event_scrapy_selector, event_obj, target_name)
        type(event_scrapy_selector, event_obj, target_name)

        event_obj.edit_date = datetime.utcnow()
        dbsession.commit()


