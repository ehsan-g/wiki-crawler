
def xpather2(website, event):
    websites = {
        'tiwall': {
            'website': 'https://www.tiwall.com',
        }
    }
    crawl_role_objs = event.positions
    i = 0
    for crawl_role_obj in crawl_role_objs:
        role = crawl_role_obj.unsorted_role
        websites[website].update({'event_people_xpath' + str(i): '//label[contains(text(),'+ '"%s"' % role + ')]/following-sibling::text()'},)
        websites[website].update({'role' + str(i): role})
        i += 1
    # //label[contains(text(),'+ '"%s"' % role + '')]/following-sibling::text()
    xpaths = websites[website]
    return xpaths


def xpather(website):
    websites = {
        'tiwall': {
            'website': 'https://www.tiwall.com',
            'event_xpath': '//*[@class = "item-page"]',
            'event_url_xpath': '//*[@class = "item-page"]//@href',
            'event_title_xpath': '//*[@class = "title attached"]//h1/text()',
            'event_poster_xpath': '//*[@class = "item-image"]/@src',
            'event_cover_xpath': '//*[@class = "cover-image"]/@src',
            'event_description_xpath': '',
            'event_loc_name_xpath': '//*[@class = "box_large page event"]'
                                    '//a[contains(@href,"/showcase?filters=venue_id")]/text()',
            'event_loc_city_xpath': '//h6[text() = "شهر:"]/following-sibling::a/text()',
            'event_loc_address_xpath': '//label[text() = "نشانی:"]/following-sibling::text()',
            'event_lat_long_xpath': '//*[@id="place-map"]/@src',
            'event_date_xpath': '//*[@class = "theater-date"]/text()',
            'event_organizer_xpath': '',
            'event_sponsor_xpath': '',
            'event_gallery_xpath': '',
            'event_position_xpath': '//*[@class="box_large page event"]//label/text()',
            'event_type_xpath': '//*[@class="title attached"]//h1/small/text()',
            'event_cat_xpath': '//h6[text() = "دسته‌بندی:"]/following-sibling::a/text()',
            'event_genre_xpath': '//h6[text() = "سبک:"]/following-sibling::a/text()',
            'start_page_url': 'https://www.tiwall.com/showcase?filters=s:theater&p=',
            'start_from_page': 1,
            'google_map': '//*[@id="place-map"]',
        },
        'tik8': {
            'website': 'https://www.tik8.com',
            'event_url_xpath': '',
        },
    }

    xpaths = websites[website]
    return xpaths