import enum
from datetime import datetime
from sqlalchemy import ForeignKey
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import force_auto_coercion, URLType, ChoiceType

force_auto_coercion()
db = SQLAlchemy()


class LocationType(enum.Enum):
    past = 1
    online = 2
    search = 3
    new = 4


class ShedualeType(enum.Enum):
    daily = 1
    weekly = 2
    monthly = 3


class OccurType(enum.Enum):
    same_day = 0
    next_day = 1
    second_day = 2
    third_day = 3
    fourth_day = 4
    fifth_day = 5
    sixth_day = 6


class WeeklyType(enum.Enum):
    saturday = 1
    sunday = 2
    monday = 3
    tuesday = 4
    wednesday = 5
    thursday = 6
    friday = 7


class MonthlyType(enum.Enum):
    first_day = 1
    second_day = 2
    third_day = 3
    fourth_day = 4
    fifth_day = 5
    sixth_day = 6
    thirty_first_day = 31


class RepeatType(enum.Enum):
    first_day = 1
    second_day = 2
    third_day = 3
    fourth_day = 4


class TicketType(enum.Enum):
    paid = 1
    free = 2
    donation = 3


'''-------------------------------------------  The Tables  ------------------------------------------------------'''


class Event(db.Model):
    __tablename__ = "events"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String)
    link = db.Column(URLType, unique=True)
    private = db.Column(db.Boolean, default=False)  # Private = True, Public = False
    poster = db.Column(URLType, unique=True)
    cover = db.Column(URLType, unique=True)
    description = db.Column(db.String)
    locations = db.relationship("Location", backref="events", lazy=True)
    schedules = db.relationship("Schedule", backref="events", lazy=True)
    people = db.relationship("Person", backref="events", lazy=True)
    organizers = db.relationship("Organizer", backref="events", lazy=True)
    sponsors = db.relationship("Sponsor", backref="events", lazy=True)
    gallery = db.relationship("Gallery", backref="events", lazy=True)
    positions = db.relationship("CrawledRole", backref="events", lazy=True)
    types = db.relationship("Type", backref="events", lazy=True)
    page = db.Column(db.Integer)  # the page the event was found
    crawled_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    edit_date = db.Column(db.DateTime)


class Person(db.Model):
    __tablename__ = "people"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    crawls = db.relationship("CrawledRole", backref="people", lazy=True, secondary="people_crawls")
    wiki_roles = db.relationship("WikiRole", backref="people", lazy=True, secondary="people_wikis")


class CrawledPerson(db.Model):
    __tablename__ = "people_crawls"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    crawl_id = db.Column(db.Integer, db.ForeignKey("crawled_roles.id"), nullable=True)
    person_id = db.Column(db.Integer, db.ForeignKey("people.id"), nullable=True)


class WikiPerson(db.Model):
    __tablename__ = "people_wikis"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    wiki_id = db.Column(db.Integer, db.ForeignKey("wikiroles.id"), nullable=True)
    person_id = db.Column(db.Integer, db.ForeignKey("people.id"), nullable=True)


# ------------------------------------------ Crawl Wiki -----------------------------------


class CrawledWiki(db.Model):
    __tablename__ = "wikis_crawls"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    crawl_id = db.Column(db.Integer, db.ForeignKey("crawled_roles.id"), nullable=True, primary_key=True)
    wiki_role_id = db.Column(db.Integer, db.ForeignKey("wikiroles.id"), nullable=True, primary_key=True)


class WikiRole(db.Model):
    __tablename__ = "wikiroles"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    wiki_role = db.Column(db.String, unique=True)
    crawls = db.relationship("CrawledRole", backref="wikiroles", lazy=True, secondary="wikis_crawls")
    wiki_people = db.relationship("Person", backref="wikiroles", lazy=True, secondary="people_wikis")


class CrawledRole(db.Model):
    __tablename__ = "crawled_roles"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False)
    unsorted_role = db.Column(db.String)  # ... ,'بازیگر‍‍', 'آهنگساز'
    wiki_roles = db.relationship("WikiRole", backref="crawled_roles", lazy=True, secondary="wikis_crawls")
    crawl_people = db.relationship("Person", backref="crawled_roles", lazy=True, secondary="people_crawls")


# ----------------------------------------------------------------------------------------


class Ticket (db.Model):
    __tablename__ = "tickets"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False, unique=True)
    type = db.Column(ChoiceType(TicketType, impl=db.Integer()))
    quantity = db.Column(db.Integer)
    price = db.Column(db.Integer)


class Organizer(db.Model):
    __tablename__ = "organizers"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False, unique=True)
    name = db.Column(db.String)
    logo = db.Column(db.String)
    info = db.Column(db.String)
    instagram = db.Column(URLType)
    facebook = db.Column(URLType)
    telegram = db.Column(URLType)
    linkedin = db.Column(URLType)


class Sponsor(db.Model):
    __tablename__ = "sponsors"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False, unique=True)
    name = db.Column(db.String)
    logo = db.Column(db.String)
    info = db.Column(db.String)
    instagram = db.Column(URLType)
    facebook = db.Column(URLType)
    telegram = db.Column(URLType)
    linkedin = db.Column(URLType)


class Gallery(db.Model):
    __tablename__ = "galleries"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False, unique=True)
    images = db.Column(db.ARRAY(URLType))
    image_titles = db.Column(db.ARRAY(db.String))


class Location(db.Model):
    __tablename__ = "locations"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False, unique=True)
    location_name = db.Column(db.String)
    online_link = db.Column(URLType)
    city = db.Column(db.String)
    address = db.Column(db.String)
    lat = db.Column(db.DECIMAL)
    long = db.Column(db.DECIMAL)


class Type(db.Model):
    __tablename__ = "types"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False)
    event_types = db.Column(db.ARRAY(db.String))  # نمایش, کنسزت, ...
    genres = db.Column(db.ARRAY(db.String))  # dram, horror, comedy, ...
    categories = db.Column(db.ARRAY(db.String))  # political, religion, musical, ...


class Schedule(db.Model):
    __tablename__ = "schedules"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, ForeignKey('events.id'), nullable=False, unique=True)
    start_date = db.Column(db.Date)
    end_date = db.Column(db.Date)
    start_time = db.Column(db.Time)
    end_time = db.Column(db.Time)
    end_time_date = db.Column(ChoiceType(OccurType, impl=db.Integer()))  # same day, Next day, nth day -- for all types
    type = db.Column(ChoiceType(ShedualeType, impl=db.Integer()))  # daily, Weekly and Monthly
    week_day_type = db.Column(ChoiceType(WeeklyType, impl=db.Integer()))  # which day of the week. Friday, Monday
    mon_day_type = db.Column(ChoiceType(MonthlyType, impl=db.Integer()))  # 1st to 31th
    mon_weekday_type = db.Column(ChoiceType(RepeatType, impl=db.Integer()))  # every 2nd Sunday of the month


# after you have defined all your models, call configure_mappers:
db.configure_mappers()
