import re
from khayyam import JalaliDate
from tiwall.tiwall.spiders.event_finder import *
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine
from models import *
from threading import Thread
from flask import Flask, render_template, request, session, redirect
import os

app = Flask(__name__)
app.secret_key = '|lil0o0^obBo0II%|\|ilioiL|oIoo0|li110||oi|I#!0oi0!I\|!I1iKL11lL||ii|'

app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://tiwall:tiwallina@localhost/tiwall"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db.init_app(app)

engine = create_engine("postgresql://tiwall:tiwallina@localhost/tiwall")
dbsession = scoped_session(sessionmaker(bind=engine))

'''
This is how we retrieve data:

    wiki_obj = dbsession.query(WikiRole).get(1)
    crawl_obj = dbsession.query(CrawledRole).get(1)
    print(wiki_obj.wiki_role)
    print(crawl_obj.wiki_role[0].wiki_role)
    
    both print the same string
'''

''' ver. 0.8.0 '''


# to change jinja2 date to persian
@app.template_filter('persian')
def date_filter(d):
    try:
        return JalaliDate(d).strftime("%A %d %B %Y")
    except:
        return


@app.route('/')
def event():
    events = dbsession.query(Event).all()
    return render_template('event.html', events=events)


@app.route('/role', methods=['GET'])
def role():
    s = status()
    crawl_objs = dbsession.query(CrawledRole).order_by(CrawledRole.unsorted_role.asc()).all()
    wiki_objs = dbsession.query(WikiRole).all()
    return render_template('roles.html', status=s, all_crawls=crawl_objs, wiki_roles=wiki_objs)


@app.route('/people/<skip>/<take>')
def people(skip, take):
    # to have 1 as my first page
    skip = int(skip)-1
    skip = int(take) * int(skip)
    count = dbsession.query(Person).count()
    if count < 20:
        take = count
    people_page = dbsession.query(Person).order_by(Person.first_name.asc()).offset(skip).limit(take)
    events_objs = dbsession.query(Event).all()
    return render_template('people.html', people_this_page=people_page, events=events_objs, skip=skip)


@app.route('/wiki', methods=['GET'])
def wiki():
    wiki_objs = dbsession.query(WikiRole).all()

    return render_template('wiki.html', wiki_objs=wiki_objs)


@app.route('/wikiroles')
def wiki_roles():
    unsorted_roles = dbsession.query(CrawledRole).filter(CrawledRole.unsorted_role).all()
    print(unsorted_roles)
    wikiroles = session.query(WikiRole).all()
    return render_template('wiki_roles.html', wikiroles=wikiroles)


@app.route('/spiders/<target_name>', methods=['POST'])
def crawl(target_name):
    s = status()
    if target_name == 'tiwall':
        while s == 'Idle':
            try:
                if not session['Crawling']:
                    session["Crawling"] = True
                    s = status()
                    print(" \n\nSpiders Unleashed\n ")
                    # Run spiders at the same time of the flask app
                    Thread(target=scheduler, args=(target_name,)).start()
                    # Thread(target=crawl_cinema_tiwall).start()
                    # Thread(target=crawl_music_tiwall).start()
                    # Thread(target=crawl_gallery_tiwall).start()
                    # Thread(target=crawl_literature_tiwall).start()
                    session["Crawling"] = False

            except KeyError:
                session["Crawling"] = False
                s = status()

        return redirect('/')


@app.route("/api/roles/submit_role/", methods=["POST"])
def submit_role():
    form = request.form
    selection = form.getlist('selected')
    crawl_obj_id = form.getlist('crawl_obj.id')[0]
    crawl_obj = dbsession.query(CrawledRole).get(crawl_obj_id)
    existing_rel = dbsession.query(CrawledWiki).filter_by(crawl_id=crawl_obj_id).all()
    new_rels = []
    if selection:
        for item in selection:  # e.g: 'crawl_obj.id, crawl_obj.wiki_roles.wiki_role'
            item = re.sub('[0123456789!@#$%^&*()_+,]', '', item)
            wiki_obj = dbsession.query(WikiRole).filter_by(wiki_role=item.strip()).first()
            rel_query = dbsession.query(CrawledWiki).filter_by(crawl_id=crawl_obj.id). \
                filter_by(wiki_role_id=wiki_obj.id).first()
            # create rel if does not exist
            if not rel_query:
                new_rel = crawl_obj.wiki_roles.append(wiki_obj)
                new_rels.append(new_rel)
                dbsession.commit()
            else:
                new_rels.append(rel_query)
                print('Rel already exist')
                continue
    #  if no selection and there is a previous relation delete it.
    elif len(selection) == 0:
        if len(existing_rel) != 0:
            for rel in existing_rel:
                dbsession.delete(rel)
                dbsession.commit()
                return redirect('/')

    for rel in existing_rel:
        if rel not in new_rels:
            dbsession.delete(rel)
            dbsession.commit()

    return redirect('/role')


@app.route('/add', methods=['POST', 'GET'])
def add_position(clean_role):
    new = request.get('clean_role')
    dbsession.add(new)
    dbsession.commit()
    return print('This word was add to db: ') + clean_role


@app.route('/spiders/status', methods=['GET'])
def status():
    try:
        if session['Crawling']:
            spider_status = 'Unleashed'
        else:
            spider_status = 'Idle'
    except KeyError:
        spider_status = 'Idle'

    return spider_status


@app.route('/events/locations/<int:event_id>')
def location(event_id):
    the_event = dbsession.query(Event).get(event_id)
    return render_template("locations.html", event=the_event)

if __name__ == "__main__":
    app.secret_key = os.urandom(1)
    app.run(debug=True, host='0.0.0.0', port=1818)
