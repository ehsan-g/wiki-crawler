from cerberus import Validator
from app import dbsession
from models import *


def wikier(wiki_role):
    wiki_obj = dbsession.query(WikiRole).filter_by(wiki_role=wiki_role).first()
    if wiki_obj:
        crawls = wiki_obj.crawls
        if crawls:
            roles = []
            for crawl_obj in crawls:
                role = crawl_obj.unsorted_role
                roles.append(role)
                return roles
    else:
        wiki_obj = WikiRole(wiki_role=wiki_role)
        dbsession.add(wiki_obj)
        dbsession.commit()
        wikier(wiki_role)


role_schema = {
    'dramaturge': {'type': 'string', 'allowed': ['نمایش‌پرداز']},
    'translator': {'type': 'string', 'allowed': ['بازگردان']},
    'producer': {'type': 'string', 'allowed': ['تهیه کننده']},
    'playwright': {'type': 'string', 'allowed': ['نمایش‌نامه نویس']},
    'director': {'type': 'string', 'allowed': ['کارگردان']},
    'actor': {'type': 'string', 'allowed': ['بازیگر']},
    'movement_director': {'type': 'string', 'allowed': ['بازی‌گردان']},
    'vocal': {'type': 'string', 'allowed': ['صدا پیشه']},
    'choreographer': {'type': 'string', 'allowed': ['طراح حرکت']},
    'graphic_designer': {'type': 'string', 'allowed': ['طراح گرافیک']},
    'assistant1_director': {'type': 'string', 'allowed': ['دستیار اول کارگردان']},
    'assistant2_director': {'type': 'string', 'allowed': ['دستیار دوم کارگردان']},
    'assistants_director': {'type': 'string', 'allowed': ['دستیاران کارگردان']},
    'set_designer': {'type': 'string', 'allowed': ['طراح صحنه']},
    'composer': {'type': 'string', 'allowed': ['آهنگساز']},
    'musician': {'type': 'string', 'allowed': ['نوازنده']},
    'music_director': {'type': 'string', 'allowed': ['سرپرست موسیقی']},
    'virtual_actor': {'type': 'string', 'allowed': ['بازیگر مجازی']},
    'makeup_artist': {'type': 'string', 'allowed': ['طراح گریم']},
    'costume_designer': {'type': 'string', 'allowed': ['طراح لباس']},
    'singer': {'type': 'string', 'allowed': ['خواننده']},
    'carpenter': {'type': 'string', 'allowed': ['ساخت دکور']},
    'script_supervisor': {'type': 'string', 'allowed': ['منشی صحنه']},
    'stage_manager': {'type': 'string', 'allowed': ['مدیر صحنه']},
    'video': {'type': 'string', 'allowed': ['ساخت ویدیو']},
    'photographer': {'type': 'string', 'allowed': ['عکاس']},
    'pr': {'type': 'string', 'allowed': ['روابط عمومی']},
    'media_advisor': {'type': 'string', 'allowed': ['مشاور رسانه']},
    'line_producer': {'type': 'string', 'allowed': ['مجری طرح']},
    'production_manager': {'type': 'string', 'allowed': ['مدیر تولید']},
    'marketing_director': {'type': 'string', 'allowed': ['مدیر تبلیغات']},
    'marketing_team': {'type': 'string', 'allowed': ['تیم تبلیغات']},
    'lighting_designer': {'type': 'string', 'allowed': ['طراح نور']},
    'audio_engineer': {'type': 'string', 'allowed': ['مهندسی صدا']},
    'sound_designer': {'type': 'string', 'allowed': ['طراح صدا']},
    'consultant': {'type': 'string', 'allowed': ['مشاور']},
    'planner': {'type': 'string', 'allowed': ['برنامه ریز']},
    'cameraman': {'type': 'string', 'allowed': ['فیلم‌بردار']},
    'asm': {'type': 'string', 'allowed': ['دستیار مجری صحنه']},
    'wardrobe': {'type': 'string', 'allowed': ['مسئول لوازم صحنه']},
    'usher': {'type': 'string', 'allowed': ['مسئول اسکان تماشاچیان']},
    'backstage': {'type': 'string', 'allowed': ['پشت صحنه']},
    'set_dresser': {'type': 'string', 'allowed': ['صحنه آرا']},
    'charge_artist': {'type': 'string', 'allowed': ['سرپرست نقش‌بندان']},
    'dancer': {'type': 'string', 'allowed': ['حرکات موزون']},
    'electrician': {'type': 'string', 'allowed': ['الکتریسین']},
    'master_electrician': {'type': 'string', 'allowed': ['سرپرست فنی']},
    'paint_crew': {'type': 'string', 'allowed': ['نقش‌بند صحنه']},
    'playbill_writer': {'type': 'string', 'allowed': ['طراح بروشور']},
    'property_master': {'type': 'string', 'allowed': ['مسئول اسباب']},
    'technical_director': {'type': 'string', 'allowed': ['مدیر فنی']},
    'theatrical_technician': {'type': 'string', 'allowed': ['مسئول فنی سالن']},
    'artistic_director': {'type': 'string', 'allowed': ['کارگردان هنری']},
    'theater_manager': {'type': 'string', 'allowed': ['مدیر مجموعه']},
    'audience_services_dir': {'type': 'string', 'allowed': ['خدمات تماشاچیان']},
    'house_manager': {'type': 'string', 'allowed': ['مسئول خانه نمایش']},
    'ticketing_agent': {'type': 'string', 'allowed': ['فروشنده بلیط']},
    'janitor': {'type': 'string', 'allowed': ['خدمات و پشتیبانی']},
    'dresser': {'type': 'string', 'allowed': ['جامه دار']},
    'fly_crew': {'type': 'string', 'allowed': ['تیم پشتیبان بندبازی']},
    'light_board_operator': {'type': 'string', 'allowed': ['روشنگر']},
    'grips': {'type': 'string', 'allowed': ['عوامل صحنه']},
    'call_boy': {'type': 'string', 'allowed': ['هماهنگی']},

          }
v = Validator()
