from khayyam import JalaliDate, JalaliDatetime, TehranTimezone

print(JalaliDatetime(1394, 5, 1, 17, 45, 40).strftime('%C'))
print(JalaliDatetime(1394, 4, 31))

now = JalaliDatetime.today()
print('1\t' + now.strftime('%a %d %B %y'))
print('2\t' + now.strftime('%A %D %B %Y'))
print('3\t' + now.strftime('%Y'))
print('4\t' + now.astimezone(TehranTimezone()).strftime('%A %d %B %Y %Z'))
print(JalaliDatetime.strptime('چهارشنبه ۳۱ تیر ۱۳۹۴ ۰۵:۴۵:۴۰ ب.ظ', '%C'))
print(JalaliDatetime.strptime('۱۳۹۴', '%Y'))

